const EnumUtil = require('ts-enum-util').$enum;

enum Color {
  green,
  yellow,
  red,
}

enum Direction {
  forward,
  backward,
}

enum Language {
  russian,
  english,
}

type ColorLanguages = Record<Language, string>;
type ColorNames = Record<Color, ColorLanguages>;

interface ITrafficLight {
  language: Language;
  isBroken?: boolean;
}

const colorNames: ColorNames = {
  [Color.green]: {
    [Language.russian]: 'зеленый',
    [Language.english]: 'green',
  },
  [Color.yellow]: {
    [Language.russian]: 'желтый',
    [Language.english]: 'yellow',
  },
  [Color.red]: {
    [Language.russian]: 'красный',
    [Language.english]: 'red',
  },
};
const firstColor = 0;
const lastColor = EnumUtil(Color).size - 1;

export class TrafficLight {
  private direction: Direction;
  private color: Color;
  private readonly language: Language;
  private readonly isBroken: boolean;

  constructor(info: ITrafficLight) {
    this.direction = Direction.forward;
    this.color = Color.green;
    this.language = info.language;
    if (info.isBroken) {
      this.isBroken = info.isBroken;
    }
  }

  nextColor() {
    if (this.isBroken) {
      return colorNames[Color.yellow][this.language];
    } else {
      switch (this.direction) {
        case Direction.forward:
          this.color++;
          if (this.color === lastColor) {
            this.direction = Direction.backward;
          }
          break;
        case Direction.backward:
          this.color--;
          if (this.color === firstColor) {
            this.direction = Direction.forward;
          }
          break;
      }
      return colorNames[this.color][this.language];
    }
  }
}
export class BrokenTrafficLight extends TrafficLight {
  constructor(info: ITrafficLight) {
    super({ language: info.language, isBroken: info.isBroken });
  }
}
