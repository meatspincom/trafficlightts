function Example() {
  this.world = 'World';
}

Example.prototype.sayHi = function() {
  console.log('Hello ' + this.world);
};

var es5Class = new Example();
es5Class.sayHi();
