const term = require('terminal-kit').terminal;
import { TrafficLight, BrokenTrafficLight } from './trafficLight';

let language;

term.cyan('Выберите язык.\n');
term.cyan('Choose language.\n');

const languages = ['Русский', 'English'];

enum Languages {
  russian,
  english,
}

enum Phrases {
  request,
  error,
  trafficLight,
  trafficLightUndefined,
}

type Phrase = Record<Phrases, PhraseObj>;

type PhraseObj = Record<Languages, string>;

const phrases: Phrase = {
  [Phrases.request]: {
    [Languages.russian]: 'Введите количество светофоров [1-9] \n',
    [Languages.english]: 'Enter the number of traffic lights [1-9] \n',
  },
  [Phrases.error]: {
    [Languages.russian]: 'Неверное значение\n',
    [Languages.english]: 'Incorrect value\n',
  },
  [Phrases.trafficLight]: {
    [Languages.russian]: 'Светофор',
    [Languages.english]: 'Traffic light',
  },
  [Phrases.trafficLightUndefined]: {
    [Languages.russian]: 'Такого светофора нет',
    [Languages.english]: 'There is no such traffic light',
  },
};

const quantitnyQuestion = () => {
  term.green(phrases[Phrases.request][language]);
  term.inputField((error, input) => {
    if (input / 2 === NaN || input <= 0 || input > 9 || input % 1 != 0) {
      term.red(`\n${phrases[Phrases.error][language]}`);
      quantitnyQuestion();
    } else {
      mainFunc(input);
    }
  });
};

const mainFunc = input => {
  term('\n');
  const parsedInput = parseInt(input);
  let trafficLightArr = new Array(parsedInput);
  for (let i = 0; i <= parsedInput; i++) {
    trafficLightArr[i] =
      Math.random() > 0.5
        ? new BrokenTrafficLight({ language, isBroken: true })
        : new TrafficLight({ language });
  }
  term.grabInput();
  term.on('key', (keyName: string) => {
    const intKeyName = parseInt(keyName);
    if (keyName === 'CTRL_C') {
      process.exit();
    } else if (!isNaN(intKeyName) && intKeyName !== 0) {
      if (trafficLightArr[keyName] === undefined) {
        console.log(phrases[Phrases.trafficLightUndefined][language]);
      } else {
        console.log(
          `${
            phrases[Phrases.trafficLight][language]
          } ${intKeyName} ${trafficLightArr[intKeyName - 1].nextColor()} `,
        );
      }
    }
  });
};

term.singleColumnMenu(languages, (error, response) => {
  language = response.selectedIndex;
  quantitnyQuestion();
});
